// Loading required libraries
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class JavaForm extends HttpServlet{

	public void doGet(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException
					{
		// JDBC driver name and database URL
		final String JDBC_DRIVER="com.mysql.jdbc.Driver";  
		final String DB_URL="jdbc:mysql://localhost/TEST";

		//  Database credentials
		final String USER = "root";
		final String PASS = "newpassword";

		// Set response content type
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String title = "Database Result";
		String docType =        
				"<!doctype html public \"-//w3c//dtd html 4.0 " +
						"transitional//en\">\n";
		out.println(docType +
				"<html>\n" +
				"<head><title>" + title + "</title></head>\n" +
				"<body bgcolor=\"#f0f0f0\">\n" +
				"<h1 align=\"center\">" + title + "</h1>\n");
		Connection conn = null;
		Statement stmt = null;
		try{
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// Open a connection
			conn = DriverManager.getConnection(DB_URL,USER,PASS);

			String ID = request.getParameter("id");
			String AGE = request.getParameter("age");
			String FIRST = request.getParameter("first_name");
			String LAST = request.getParameter("last_name");

			// Execute SQL query

			stmt = conn.createStatement();
			String sql = "SELECT id FROM Employees";
			String redirect = "<a href="+ "http://192.168.26.77:8080/test/" + ">BACK</a>";
			int idnum = Integer.parseInt(ID);
			int flag =0;
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while(rs.next()){
				int id1  = rs.getInt("id");
				if (id1 == idnum)
				{
					flag=1;
				}
			}

			if (flag==1)
			{
				out.println("Duplicate Data...Data not saved...<br>");
				out.println(redirect + "</body></html>");
				rs.close();
			}
			else
			{
				String ex = "insert into Employees values ("+ID+","+AGE+","+"'"+FIRST+"'"+","+"'"+LAST+"'"+")";
				stmt.executeUpdate(ex);

				out.println("Data Saved....<br>");
				out.println(redirect + "</body></html>");

				stmt.close();
				conn.close();
				rs.close();

				out.println("</body></html>");
			}

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		} //end try
					}
} 