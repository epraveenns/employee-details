// Loading required libraries
import java.io.*;	
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class EmpSearch extends HttpServlet{

	public void doGet(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException
					{
		// JDBC driver name and database URL
		final String JDBC_DRIVER="com.mysql.jdbc.Driver";  
		final String DB_URL="jdbc:mysql://localhost/TEST";

		//  Database credentials
		final String USER = "root";
		final String PASS = "newpassword";

		// Set response content type
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String title = "Database Result";
		String redirect = "<a href="+ "http://192.168.26.77:8080/test/" + ">BACK</a>";
		String docType =
				"<!doctype html public \"-//w3c//dtd html 4.0 " +
						"transitional//en\">\n";
		out.println(docType +
				"<html>\n" +
				"<head><title>" + title + "</title></head>\n" +
				"<body bgcolor=\"#f0f0f0\">\n" +
				"<h1 align=\"center\">" + title + "</h1>\n");
		Connection conn = null;
		Statement stmt = null;
		try{
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// Open a connection
			conn = DriverManager.getConnection(DB_URL,USER,PASS);

			// Execute SQL query
			stmt = conn.createStatement();

			int flag = 0; 
			String m = request.getParameter("searchID");
			String sql = "SELECT id, first, last, age FROM Employees";

			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while(rs.next()){
				//Retrieve by column name
				int id1  = rs.getInt("id");
				if (id1 == Integer.parseInt(m))
				{
					int age1 = rs.getInt("age");
					String first1 = rs.getString("first");
					String last1 = rs.getString("last");

					//Display values
					out.println("ID: " + id1);
					out.println(", Age: " + age1);
					out.println(", First: " + first1);
					out.println(", Last: " + last1 + "<br>");

					flag=1;
				}
				

			}
			out.println(redirect + "</body></html>");
			if (flag==0)
			{
				out.println("Employee Doesnt Exist");
				out.println(redirect + "</body></html>");
			}
			out.println("</body></html>");

			// Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		} //end try
					}
} 